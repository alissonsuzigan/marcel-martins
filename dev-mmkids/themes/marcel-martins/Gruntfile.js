module.exports = function(grunt) {
  "use strict";

  // Setting webstore options
  grunt.config.set("options", {
      settings : grunt.file.readJSON("config/settings.json")
    }
  );

  // Init Grunt
  require("time-grunt")(grunt);
  require("load-grunt-config")(grunt, {
    configPath: process.cwd() + "/config/grunt/",
    init: true
  });
};