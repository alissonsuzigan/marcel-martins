<?php

// Enqueue styles and scripts :::::::::::::::::::::::::::::::::::::::::::::::::
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
function theme_enqueue_scripts() {
	// styles
	wp_enqueue_style('style', get_bloginfo('template_url').'/css/style.min.css', null, null);
	// scripts
	wp_enqueue_script('startup', get_bloginfo('template_url').'/js/startup.min.js', null, null, false);
	wp_enqueue_script('vendor', get_bloginfo('template_url').'/js/vendor/jquery/jquery-3.0.0.min.js', null, null, true);
	wp_enqueue_script('global', get_bloginfo('template_url').'/js/global.min.js', null, null, true);
	wp_enqueue_script('livereload', 'http://localhost:35729/livereload.js?snipver=1', null, false, true);
}

// Add Featured Image Support :::::::::::::::::::::::::::::::::::::::::::::::::
add_theme_support('post-thumbnails');

// Clean up the <head> ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
add_action('init', 'removeHeadLinks');
function removeHeadLinks() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'wp_generator');
}

// Register menus :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
add_action('init', 'register_menus' );
function register_menus() {
	register_nav_menus(
		array(
			'main-nav' => 'Main Navigation',
			'secondary-nav' => 'Secondary Navigation',
			'sidebar-menu' => 'Sidebar Menu'
		)
	);
}

// Register widgets ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
add_action('widgets_init', 'register_widgets');
function register_widgets(){
	register_sidebar( array(
		'name' => __( 'Sidebar' ),
		'id' => 'main-sidebar',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}


// Register custom images :::::::::::::::::::::::::::::::::::::::::::::::::::::
add_action( 'init', 'image_custom_size' );
function image_custom_size() {
  add_image_size('small-card', 450, 450, true);
  add_image_size('list-card', 1000, 400, true);
  add_image_size('post-image', 950, 950);
  add_image_size('full-image', 2000, 800, true);
}


// Disable use XML-RPC :::::::::::::::::::::::::::::::::::::::::::::::::::::::
add_filter( 'xmlrpc_enabled', '__return_false' );
// Disable X-Pingback to header
add_filter( 'wp_headers', 'disable_x_pingback' );
function disable_x_pingback( $headers ) {
  unset( $headers['X-Pingback'] );
	return $headers;
}