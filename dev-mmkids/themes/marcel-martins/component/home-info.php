<?php if ( have_posts()) {
  the_post();
  if ( get_the_content() != '') {
  ?>
    <div id="home-info" class="home-info">
      <div class="content-width">

        <div class="home-info-content">
          <?php
            the_content();
            edit_post_link('Editar '. get_the_title());
           ?>
        </div>
        <?php get_template_part('component/share'); ?>

      </div>
    </div>

    <?php
    }
  }
?>