<article id="single-album" <?php post_class('single-album'); ?>>
  <?php if ( have_posts() ) : the_post(); ?>

    <div class="featured-image">
      <div class="single-full-image">
        <?php the_post_thumbnail('full-image'); ?>
      </div>
    </div>

    <div class="content-width">
      <div class="single-album-content">

        <h1 class="single-title">
          <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
            <span class="title"><?php the_title(); ?></span>
            <span class="date"><?php the_date(); ?></span>
          </a>
        </h1>

        <?php get_template_part('component/share'); ?>

        <div class="single-text">
          <?php the_content(); ?>
        </div>
      </div>

      <div class="single-navigation">
        <?php
          the_post_navigation( array(
            'prev_text' => '<span class="meta-nav" aria-hidden="true">Álbum anterior</span>',
            'next_text' => '<span class="meta-nav" aria-hidden="true">Próximo álbum</span>',
            'screen_reader_text' => 'Veja outros álbuns:'
          ));
        ?>
      </div>
    </div>

  <?php endif; ?>
</article>
