<article id="card-<?php the_ID(); ?>" <?php post_class('card'); ?>>

  <figure class="card-thumb">
    <a class="card-thumb-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
      <?php the_post_thumbnail('list-card'); ?>
    </a>
  </figure>

  <div class="card-content">
    <header class="card-header">
      <h2 class="card-title">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
          <span class="title"><?php the_title(); ?></span>
          <span class="date"> - <?php the_date(); ?></span>
        </a>
      </h2>
    </header>
    <?php edit_post_link('Editar '. get_the_title()); ?>
  </div>

</article>
