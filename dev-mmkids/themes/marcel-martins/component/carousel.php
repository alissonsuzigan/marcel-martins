<div id="carousel-slider" class="carousel-slider">
  <div id="carousel" class="carousel">
    <?php
      $fields = CFS()->get('carousel');
      foreach ( $fields as $field ) {
        echo wp_get_attachment_image($field['image'], 'full-image');
      }
    ?>
  </div>
</div>