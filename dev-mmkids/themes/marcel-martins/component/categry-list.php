<div id="category-list" class="category-list">
  <div class="content-width">

    <h1 class="page-title"><?php single_cat_title(); ?></h1>
    <div class="category-list-content">
      <?php
        if ( have_posts() ) :
          while ( have_posts() ) : the_post();
            get_template_part( 'component/card' );
          endwhile;
        endif;
      ?>
    </div>

    <div class="card-navigation">
      <?php previous_posts_link("Álbuns recentes"); ?>
      <?php next_posts_link("Mais álbuns"); ?>
    </div>

  </div>
</div>
