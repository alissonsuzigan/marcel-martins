<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes() ?>><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" <?php language_attributes() ?>><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" <?php language_attributes() ?>><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes() ?>><!--<![endif]-->

<head>
  <meta charset="<?php bloginfo( 'charset' ) ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <!-- <meta name="viewport" content="width=device-width"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php bloginfo('name') ?> - <?php bloginfo('description') ?></title>
  <?php wp_head() ?>
  <link href='https://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Ubuntu:300,400,700' rel='stylesheet' type='text/css'>
</head>

<body <?php body_class() ?>>
  <span class="display"></span>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6&appId=178341945566021";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>


  <header id="page-header" class="page-header">
    <div class="header-bar clearfix">

      <div class="content-header">
        <div class="header-branding">
          <?php if (is_front_page()) : ?>
            <h1 class="header-title"><a class="header-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
          <?php else : ?>
            <p class="header-title"><a class="header-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
          <?php endif; ?>
          <button class="toggle-menu"><i class="icon-menu"></i></button>
        </div>

        <?php wp_nav_menu(array(
          'container'       => 'nav',
          'container_id'    => 'main-nav',
          'container_class' => 'main-nav',
          'menu_id'         => 'menu-list',
          'menu_class'      => 'menu-list'
        )) ?>
      </div>

    </div>
  </header>

  <div id="page-content" class="page-content">