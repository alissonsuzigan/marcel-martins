// CssMin definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    default: {
      expand: true,
      cwd: "css/",
      src: ["*.css", "!*.min.css"],
      dest: "css/",
      ext: ".min.css"
    }
  }
};