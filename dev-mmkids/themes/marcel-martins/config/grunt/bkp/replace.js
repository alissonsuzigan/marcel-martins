// Replace definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    desktopDevelopment: {
      src: [
        "static/script/startup.js"
      ],
      overwrite: true,
      replacements: [
        {
          from: /HTTP_IMAGES_PATH\: \".*\"/,
          to: "HTTP_IMAGES_PATH: \"" +
            options.desktop.sass.development.httpPath +
            options.desktop.sass.development.imagesDir + "\""
        },
        {
          from: /https?:\/\/acesso\.(qa\.|dev\.)?vmcommerce\.intra/g,
          to: "http://acesso.dev.vmcommerce.intra"
        },
        {
          from: /https?\:\/\/connect\.(walmart|(stg\.)?waldev)\.com.br/g,
          to: "http://acesso.dev.vmcommerce.intra"
        },
        {
          from: /THUMB_URL\: \".*\"/,
          to: "THUMB_URL: \"//static-cms.waldev.com.br/\""
        }
      ]
    }

  }
};