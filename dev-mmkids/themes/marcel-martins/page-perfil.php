<?php
/**
 * Template Name: Perfil
 *
 * @package WordPress
 * @subpackage Marcel Martins
 * @since Marcel Martins 1.0
 */

  get_header();
   get_template_part('component/perfil');
  get_footer();
?>