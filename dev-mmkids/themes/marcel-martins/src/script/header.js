app.header = (function() {
  "use strict";

  var header = $("#page-header"),
      headerBar = header.find(".header-bar"),
      height = headerBar.height(),
      scrollPrev = window.scrollY;


  function checkScrollPosition() {
    if (window.scrollY > height) {
      headerBar.addClass("fixed");

    } else if (window.scrollY === 0) {
      headerBar.removeClass("fixed animate");
    }
  }


  function checkScrollDirection() {
    if (window.scrollY > height) {
      if (scrollPrev <= window.scrollY) {
        headerBar.css("top", -height -5);

      } else {
        headerBar.addClass("animate").css("top", 0);
      }
    }
    scrollPrev = window.scrollY;
  }


  function checkHeaderHeight() {
    height = headerBar.outerHeight();
    header.height(height);
  }


  function bindEvents() {
    $(".toggle-menu").on("click", function () {

      if($("#main-nav").height() === 0) {
        $("#main-nav").height( $("#menu-list").height() );
      } else {
        $("#main-nav").height(0);
      }
    });

    // window.onscroll = function() {
    //   checkScrollPosition();
    //   checkScrollDirection()
    // }
    // window.onresize = function() {
    //   checkHeaderHeight();
    // }
  }


  return {
    init: function() {
      // header.height(height -8);
      // checkHeaderHeight();
      bindEvents();
    }
  };
})();
app.header.init();