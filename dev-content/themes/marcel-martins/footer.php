    </div>

		<footer id="page-footer" class="page-footer">

      <div class="footer-menu">
        <div class="content-width">
          <?php wp_nav_menu(array(
            'container'       => 'nav',
            'container_id'    => 'footer-nav',
            'container_class' => 'footer-nav',
            'menu_id'         => 'menu-list',
            'menu_class'      => 'menu-list'
          )) ?>
        </div>
      </div>

      <div class="content-width">
        <div class="footer-info">

          <p class="footer-title"><a href="<?php echo esc_url(home_url( '/' )); ?>" rel="home"><?php bloginfo('name'); ?></a></p>
          <p class="contact-info">11.3412.0636 - 11.98181.6205 - <a href="mailto:fotos@marcelmartins.com.br">fotos@marcelmartins.com.br</a></p>
          <p class="copyright">© Imagens com direitos reservados</p>

          <?php get_template_part('component/share'); ?>

        </div>
      </div>

		</footer>
		<?php wp_footer() ?>


  </body>
</html>
