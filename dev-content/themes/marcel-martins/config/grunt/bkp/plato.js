// Plato definitions
module.exports = function(grunt) {
  var options  = grunt.config.get("options");
  return {
    task: {
      options : {
        exclude: /.*\/vendor\/.*|\.min.js|.*self-help.*|.*selfhelp.*|.*sign-.*|.*call-center.*|.*captcha.*|.*my-account.*/,
        jshint : false
      },
      files: {
        "static/reports": "<%= jshint.files %>"
      },
    },
  }
};