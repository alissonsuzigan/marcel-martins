<?php
/**
 * Template Name: Home
 *
 * @package WordPress
 * @subpackage Marcel Martins
 * @since Marcel Martins 1.0
 */

  get_header();
  get_template_part('component/carousel');
  get_template_part('component/home-info');
  get_template_part('component/featured');
  get_template_part('component/instagram');
  get_footer();
?>