app.area = (function() {
  "use strict";

  var area = $("#area"),
      content = area.find(".content-area");


  function loadAllAreas() {
    var i, menuLength = app.menuData.length;

    for (i=0; i< app.pageId; i++) {
      content.before("<div class='content-area item-"+ i +" hide' data-id='"+ i +"'></div>");
      area.find(".item-"+ i).load(app.menuData[i].url + " #main");
    }

    for (i=app.pageId+1; i<menuLength; i++) {
      area.append("<div class='content-area item-"+ i +" hide' data-id='"+ i +"'></div>");
      area.find(".item-"+ i).load(app.menuData[i].url + " #main");
    }
  }


  function changeArea(newId) {
    if (newId > app.pageId) {
      app.slide.goRight(newId);
    } else {
      app.slide.goLeft(newId);
    }
  }

  // function update (newId) {
  //   changeArea(newId);
  // }

  return {
    init: function() {
      loadAllAreas();
    },
    // update : update,
    changeArea : changeArea
  };
})();
app.area.init();