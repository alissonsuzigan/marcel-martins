<div id="perfil" class="perfil">
  <?php if ( have_posts() ) : the_post(); ?>

    <div class="single-full-image">
      <?php the_post_thumbnail('full-image'); ?>
    </div>

    <div class="content-width">
      <div class="perfil-content single-album-content">

        <h1 class="single-title">
          <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
            <span class="title"><?php the_title(); ?></span>
          </a>
        </h1>

        <div class="single-text">
          <?php the_content(); ?>
        </div>
        <?php edit_post_link('Editar '. get_the_title()); ?>

      </div>
    </div>

  <?php endif; ?>
 </div>
