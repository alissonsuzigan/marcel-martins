<article id="card-<?php the_ID(); ?>" <?php post_class('card card-small'); ?>>

  <figure class="card-thumb">
    <a class="card-thumb-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
      <?php the_post_thumbnail('small-card'); ?>
    </a>
  </figure>

  <div class="card-content">
    <header class="card-header">
      <h3 class="card-title">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
      </h3>
    </header>
    <!-- <div class="card-date"><?php the_time( get_option( 'date_format' ) ); ?></div> -->
    <?php edit_post_link('Editar '. get_the_title()); ?>
  </div>

</article>