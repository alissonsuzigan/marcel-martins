<div id="featured" class="featured">
  <div class="content-width">

    <h2 class="page-title">Aniversários</h2>
    <div class="featured-content clearfix">
      <?php
        $cardQuantity = CFS()->get( 'featured-quantity' );
        query_posts( array ( 'category_name' => 'aniversario', 'posts_per_page' => $cardQuantity ) );

        if ( have_posts() ) :
          while ( have_posts() ) : the_post();
            get_template_part( 'component/card-small' );
          endwhile;
        endif;
      ?>
    </div>

    <h2 class="page-title">Ensaios</h2>
    <div class="featured-content clearfix">
      <?php
        query_posts( array ( 'category_name' => 'e-session-l-trash-the-dress', 'posts_per_page' => $cardQuantity ) );

        if ( have_posts() ) :
          while ( have_posts() ) : the_post();
            get_template_part( 'component/card-small' );
          endwhile;
        endif;
      ?>
    </div>

  </div>
</div>
