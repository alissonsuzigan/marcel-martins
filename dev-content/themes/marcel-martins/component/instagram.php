<div id="instagram" class="instagram">
  <div class="content-width">

    <h2 class="page-title">
      <a href="https://www.instagram.com/marcelmartinsfotografia/" target="_blank">
        <!-- <span class="icon-instagram2"></span> -->
        <span class="title">Instagram:</span>
        <span class="user-label">@marcelmartinsfotografia</span>
      </a>
    </h2>

    <div class="instagram-content">
      <!-- LightWidget WIDGET -->
      <script src="//lightwidget.com/widgets/lightwidget.js"></script>
      <iframe src="//lightwidget.com/widgets/e90b036924f8509ba417a4e8fd297fb8.html"
              id="lightwidget_e90b036924"
              name="lightwidget_e90b036924"
              scrolling="no"
              allowtransparency="true"
              class="lightwidget-widget"
              style="width: 100%; border: 0; overflow: hidden;">
      </iframe>
    </div>

  </div>
</div>
