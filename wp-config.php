<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'wp-marcelmartins');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'root');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', '');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hlVOVE@z^bs#Bv$)dU++<h4MowMZ;m`B.6W+7JC^9;GR@K(A>ZUp[88tM?|POH/7');
define('SECURE_AUTH_KEY',  'GXUciFiY>/3`|i,#=dAJ]Bn:K)/VnH?pAti~l +[9Mw*c/c+{%-O~b(TIY$z<NoJ');
define('LOGGED_IN_KEY',    ']7]b(K`!&I9%@PVvgSJ7H:d+|*S2liPm0c#MPyAnH<d+{B+gbUq4#Od!0)ZZCOV[');
define('NONCE_KEY',        '<8_qs2Se.=gX%7{w+72]{bW0NZ+(nvq[<s-/VwWuj#q+#P!;qWih1U=E# NzwRc|');
define('AUTH_SALT',        'E`<8psgCKX>W})xb=}dU8<z^}q[7E>gsh-l,3,GEd3#czY1CAOuGesBufk<%mU~8');
define('SECURE_AUTH_SALT', '0(T(VfRe))+evlA|ul4.s4_-=CnUKE/02 /[?)e@/:T!0km;u?C7l`d6e!z;d}Y.');
define('LOGGED_IN_SALT',   'Ee@1/.L).~>qt,*c-6;?*/S_+!71-=2!.d59#h,=e/%0DcAyYQs+jwU8NyOB&Skg');
define('NONCE_SALT',       ' +#Q-8iD|~ni9EUO3k_2!b&2yk|c$*[,wc7X~2$k)=)l&bYF#Kg/BFoNV%4zh)=y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'pt_BR');


/**
 * Set custom paths
 *
 * These are required because wordpress is installed in a subdirectory.
 */
if (!defined('WP_SITEURL')) {
	define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/marcelmartins/wordpress');
}
if (!defined('WP_HOME')) {
	define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME'] . '/marcelmartins');
}
if (!defined('WP_CONTENT_DIR')) {
	// define('WP_CONTENT_DIR', dirname(__FILE__) . '/dev-content');
	define('WP_CONTENT_DIR', dirname(__FILE__) . '/dev-mmkids');
}
if (!defined('WP_CONTENT_URL')) {
	// define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/marcelmartins/dev-content');
	define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/marcelmartins/dev-mmkids');
}


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');



// REMOVE THIS!!!

// Sets 'direct' method to auto update (without FTP)
define('FS_METHOD','direct');